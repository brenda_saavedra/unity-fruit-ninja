﻿using UnityEngine;
using System.Collections;

public class LineCreator : MonoBehaviour {

	int vertexCount = 0;
	bool mouseDown = false;
	LineRenderer line;
	public GameObject blast;

	void Awake(){
		line = GetComponent<LineRenderer> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			if (Input.touchCount > 0) {
				if (Input.GetTouch (0).phase == TouchPhase.Moved) {
					createLine ();
				}

				if (Input.GetTouch (0).phase == TouchPhase.Ended) {
					cleanColliders ();
				}
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				mouseDown = true;
			}

			if (mouseDown) {
				createLine ();
			}

			if (Input.GetMouseButtonUp (0)) {
				cleanColliders ();
			}
		}
	}

	void createLine(){
		line.SetVertexCount (vertexCount + 1);
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		line.SetPosition (vertexCount, mousePos);
		vertexCount++;

		//Adding colliders
		BoxCollider2D box = gameObject.AddComponent<BoxCollider2D>();
		box.transform.position = line.transform.position;
		box.size = new Vector2 (0.1f, 0.1f);
	}

	void cleanColliders(){
		mouseDown = false;
		line.SetVertexCount (0);
		vertexCount = 0;

		BoxCollider2D[] colliders = gameObject.GetComponents<BoxCollider2D> ();
		foreach (BoxCollider2D box in colliders) {
			Destroy (box);
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Bomb") {
			Destroy (col.gameObject);
			GameObject obj = Instantiate (blast, col.transform.position, Quaternion.identity) as GameObject;
			Destroy (obj.gameObject, 5f);
		}
	}
}
