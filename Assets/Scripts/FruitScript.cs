﻿using UnityEngine;
using System.Collections;

public class FruitScript : MonoBehaviour {

	public GameObject cutL;
	public GameObject cutR;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Line") {

			GameObject cR = Instantiate (cutR, transform.position, Quaternion.identity) as GameObject;
			GameObject cL = Instantiate (cutL, new Vector3(transform.position.x - 2f, transform.position.y, 0), cutL.transform.rotation) as GameObject;

			cR.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (2f, 2f),ForceMode2D.Impulse);
			cR.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-2f,2f),ForceMode2D.Impulse);

			cL.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-2f, 2f),ForceMode2D.Impulse);
			cL.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-2f,2f),ForceMode2D.Impulse);

			Destroy (gameObject);
		}
	}
}
